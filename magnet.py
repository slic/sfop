from slic.core.adjustable import PVAdjustable


class Magnet(PVAdjustable):

    def __init__(self, ID, accuracy=0.01):
        pvn_set  = ID + ":I-SET"
        pvn_read = ID + ":I-READ"
        super().__init__(pvn_set, pvn_read, accuracy=accuracy, ID=ID)
#        pvn_comp = ID + ":I_COMP"
#        super().__init__(pvn_set, pvn_read, pvname_moving=pvn_comp) # COMP updates only once per second, so we cannot use it as moving status



