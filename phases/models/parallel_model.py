import math
import numpy as np
from scipy.optimize import fsolve


def parallel2gap(K, phi_in_degree, undudict):
    phi = math.radians(phi_in_degree)
    gLH = K2gap(K, undudict['K-value_LH'])
    if phi >= 0.0:
        gLV = K2gap(K, undudict['K-value_LV+'])
        gC = K2gap(K, undudict['K-value_C+'])
        dgLV = gLV - gLH
        dgC = gC - gLH - dgLV/2
    else:
        gLV = K2gap(K, undudict['K-value_LV-'])
        gC = K2gap(K, undudict['K-value_C-'])
        dgLV = gLV - gLH
        dgC = gC - gLH - dgLV/2
    return gLH + dgLV * np.sin(0.5 * phi)**2 + dgC * np.sin(phi)**2


def K2gap(Kval, fitparam):
    g2K_func = np.poly1d(fitparam[::-1])
    tau_init = 1.0
    k_log = float(np.log(Kval))
    return float(fsolve(k_log - g2K_func, tau_init))



