#!/usr/bin/env python

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("finput", help="name of excel file to load")
parser.add_argument("foutput", help="name of json file to write")
clargs = parser.parse_args()


from collections import defaultdict
import pandas as pd
from slic.utils import json_save


def load(fn, nparams=6, skip_cols=3):
    engine = "xlrd" if fn.endswith(".xls") else "openpyxl"
    df = pd.read_excel(fn, engine=engine)

    header = df.columns
    data = df.values

    nrows, ncols = data.shape
    assert nrows % nparams == 0
    nunds = nrows // nparams

    res = defaultdict(dict)
    for i in range(nunds):
        start = i * nparams
        stop = start + nparams

        und_name = data[start, 1]
        idata = data[start:stop, 1:]

        for j in range(skip_cols, ncols):
            param_name = header[j]
            jdata = idata[:, j-1].astype(float)
            res[und_name][param_name] = list(jdata)

    return dict(**res)


def print_overview(data):
    unds = data.keys()
    unds = sorted(unds)

    params = data[unds[0]].keys()
    params = sorted(params)

    nunds = len(unds)
    nparams = len(params)

    print(f"read {nparams} parameters for {nunds} undulators")
    print("\nUndulators:", ", ".join(unds))
    print("\nParameters:", ", ".join(params))





if __name__ == "__main__":
    data = load(clargs.finput)
    print_overview(data)
    json_save(data, clargs.foutput)



