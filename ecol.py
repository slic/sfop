from magnets import MagnetsScaler


MAGNET_NAMES = (
    "SARCL02-MQUA130",
    "SARCL02-MQUA150",
    "SARCL02-MQSK160",
    "SARCL02-MQUA210",
    "SARCL02-MQUA250",
    "SARCL02-MQKS300",
    "SARCL02-MQUA310",
    "SARCL02-MQUA350",
    "SARCL02-MQSK420",
    "SARCL02-MQUA430",
    "SARCL02-MQUA460"
)


class EcolScaler(MagnetsScaler):

    def __init__(self, ID="ECOL-SCALER", magnet_names=MAGNET_NAMES, factor=1):
        super().__init__(ID, magnet_names, factor=factor)



