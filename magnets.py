from slic.core.adjustable import Adjustable
from slic.core.device import SimpleDevice

from magnet import Magnet


class MagnetsScaler(Adjustable):

    def __init__(self, ID, magnet_names, factor=1):
        super().__init__(ID)
        self.factor = factor
        mags = {f"m{i+1}": Magnet(n) for i, n in enumerate(magnet_names)}
        self.magnets = SimpleDevice(ID, **mags)

    def get_current_value(self):
        return self.factor

    def set_target_value(self, factor):
        old_factor = self.factor
        self.factor = factor
        tasks = [] # we collect tasks to run in parallel, ...
        for m in self.magnets:
            current = m.get()
            t = m.set(current / old_factor * factor)
            tasks.append(t)
        for t in tasks: # ... then we need to wait for all tasks to finish
            t.wait()

    def is_moving(self):
        return any(m.is_moving() for m in self.magnets)



